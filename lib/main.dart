import 'package:flutter/material.dart';
import 'package:mapa_flutter/mapa/maps.dart';
import 'package:provider/provider.dart';
import 'direcciones/Direction.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => Direction(),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'App 2da fase',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: MapaHome(),
      ),
    );
  }
}
