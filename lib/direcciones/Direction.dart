import 'package:flutter/material.dart';
import 'package:google_map_polyline/google_map_polyline.dart' as poli;
import 'package:google_maps_flutter/google_maps_flutter.dart' as maps;

class Direction extends ChangeNotifier {
  poli.GoogleMapPolyline googlePolyLine = new poli.GoogleMapPolyline(
      apiKey: "AIzaSyDGwbxT5Ga-00jhaRivrdmJvRVvg9LBWsc");
  Set<maps.Polyline> _rutas = Set();
  Set<maps.Polyline> get currentRutas => _rutas;

  
  otraDireccion(maps.LatLng from, maps.LatLng to) async {
    var origen = maps.LatLng(from.latitude, from.longitude);
    var destino = maps.LatLng(to.latitude, to.longitude);

    Set<maps.Polyline> newRutas = Set();

    List<maps.LatLng> rutaCoordenadas =
        await googlePolyLine.getCoordinatesWithLocation(
            origin: origen, destination: destino, mode: poli.RouteMode.driving);

    var linea = maps.Polyline(
      points: rutaCoordenadas,
      polylineId: maps.PolylineId("Mejor Ruta"),
      color: Colors.red,
      width: 4,
    );
    newRutas.add(linea);
    _rutas = newRutas;
    notifyListeners();
  }
}
