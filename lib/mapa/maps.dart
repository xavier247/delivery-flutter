import 'dart:async';
import 'dart:math';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get_version/get_version.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart'
    as lct; // lct referencia a la localizacion
import 'package:mapa_flutter/direcciones/Direction.dart';
import 'package:provider/provider.dart';

class MapaHome extends StatefulWidget {
  MapaState createState() => MapaState();
}

class MapaState extends State<MapaHome> {
  var _controllerGoogle = Completer();
  GoogleMapController _mapController;
  Set<Marker> listaMarkets = {};
  Marker marker;
  LatLng fromDesdeInicio = LatLng(0, 0);
  LatLng tohaciaFin = LatLng(0, 0);
  String destino = "";

  StreamSubscription _streamSubCriptor;
  lct.Location _tracker = lct.Location();
  Circle circle;

  var api;
  @override
  void initState() {
    super.initState();
    forzarUbicacion();
    // actualizacionUbicacion();
    obtenerInformacionApp();
  }

  static CameraPosition onPosicion =
      CameraPosition(target: LatLng(0, 0), zoom: 19);

  @override
  Widget build(BuildContext context) {
    api = Provider.of<Direction>(context);
    return Scaffold(
      body: Stack(
        children: [
          Consumer<Direction>(
            builder: (context, api, child) {
              return GoogleMap(
                myLocationEnabled: false, // activar ubicacion automatica
                markers: listaMarkets,
                polylines: api.currentRutas,
                onMapCreated: (controller) {
                  _controllerGoogle.complete(controller);
                },
                initialCameraPosition: onPosicion,
              );
            },
          ),
          Positioned(
            top: 40,
            right: 25,
            left: 15,
            child: Container(
              color: Colors.white,
              child: TextField(
                onChanged: (value) {
                  destino = value;
                },
                decoration: InputDecoration(
                  hintText: "Destino",
                  suffixIcon: IconButton(
                      icon: Icon(Icons.search),
                      onPressed: () {
                        buscarDireccion();
                      }),
                ),
              ),
            ),
          )
        ],
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          //forzarUbicacion();
          currenLocationCar();
        },
        label: Text("Centrar Ubicacion"),
      ),
    );
  }

  buscarDireccion() async {
    print("....................." + destino);
    try {
      List<Placemark> listPlace =
          await Geolocator().placemarkFromAddress(destino);
      tohaciaFin = LatLng(
          listPlace[0].position.latitude, listPlace[0].position.longitude);
      api.otraDireccion(fromDesdeInicio, tohaciaFin);
      setState(() {
        listaMarkets.add(
          new Marker(
            markerId: MarkerId("destino"),
            position: LatLng(tohaciaFin.latitude, tohaciaFin.longitude),
          ),
        );
        _vistaCentro();
      });
    } catch (e) {
      Fluttertoast.showToast(
          msg: "Problemas con la ruta Esceficada",
          toastLength: Toast.LENGTH_SHORT);
      print("direccion------${e.toString()}");
    }
  }

  Future<void> forzarUbicacion() async {
    Position posicion = await Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best);
    print("Latitub-----" +
        posicion.latitude.toString() +
        "Longitud------------" +
        posicion.longitude.toString());
    setState(() {
      fromDesdeInicio = LatLng(posicion.latitude, posicion.longitude);
      onPosicion = CameraPosition(
          target: LatLng(posicion.latitude, posicion.longitude), zoom: 19);
    });

    listaMarkets.add(
      Marker(
        markerId: MarkerId("Mi ubicacion "),
        draggable: true,
        position: LatLng(posicion.latitude, posicion.longitude),
        onDragEnd: (value) {
          Fluttertoast.showToast(
              msg: "...${value.longitude}...${value.latitude}",
              toastLength: Toast.LENGTH_SHORT);
        },
      ),
    );
    GoogleMapController controller = await _controllerGoogle.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(onPosicion));
  }

  Future<void> actualizacionUbicacion(int cod) async {
    StreamSubscription<Position> positionStream =
        Geolocator().getPositionStream().listen((Position position) {
      print(position == null
          ? 'Unknown'
          : position.latitude.toString() +
              ', ' +
              position.longitude.toString());

      setState(() {
        onPosicion = CameraPosition(
            target: LatLng(position.latitude, position.longitude), zoom: 19);
        Fluttertoast.showToast(
            msg:
                "Ubicacion actualizada .latitud...${position.latitude}... longitud...${position.longitude}",
            toastLength: Toast.LENGTH_SHORT);
      });
    });
    if (cod == 1) {
      positionStream.cancel();
    }
  }

  obtenerInformacionApp() async {
    String versionName;
    String versionCode;
    String versionIos;
    String appId;
    try {
      versionName = await GetVersion.projectVersion;
      versionCode = await GetVersion.projectCode;
      versionIos = await GetVersion.platformVersion;
      appId = await GetVersion.appID;
      print(" versiones de NAME---------$versionName");
      print(" versiones de CODE---------$versionCode");
      print(" versiones  de Android o IOS---------$versionIos");
      print(" ID de App ---------$appId");
    } on PlatformException {
      versionName = "Problemas al obtener la version Code";
      versionName = "Problemas al obtener la version Name";
      versionIos = "Problemas al obtener la version Code";
      appId = "Problemas al obtener la version Name";
    }
  }

  _vistaCentro() async {
    var left = min(fromDesdeInicio.latitude, tohaciaFin.latitude);
    var rigth = max(fromDesdeInicio.latitude, tohaciaFin.latitude);
    var top = max(fromDesdeInicio.longitude, tohaciaFin.longitude);
    var bottom = min(fromDesdeInicio.longitude, tohaciaFin.longitude);
    var bounds = LatLngBounds(
        southwest: LatLng(left, bottom), northeast: LatLng(rigth, top));
    var cameraUpdate = CameraUpdate.newLatLngBounds(bounds, 50);
    GoogleMapController controller = await _controllerGoogle.future;
    controller.animateCamera(cameraUpdate);
  }

  void actualizarMarkert(lct.LocationData locationData, Uint8List imagenData) {
    LatLng latLng = LatLng(locationData.latitude, locationData.longitude);
    setState(() {
      marker = Marker(
          markerId: MarkerId(""),
          position: latLng,
          rotation: locationData.heading,
          draggable: false,
          zIndex: 2,
          flat: true,
          anchor: Offset(0.5, 0.5),
          icon: BitmapDescriptor.fromBytes(imagenData));
      circle = Circle(
        circleId: CircleId("carro"),
        radius: locationData.accuracy,
        zIndex: 1,
        strokeColor: Colors.blue,
        center: latLng,
        fillColor: Colors.blue.withAlpha(70),
      );
      listaMarkets.add(marker);
    });
  }

  Future<Uint8List> getMarketCar() async {
    ByteData data = await DefaultAssetBundle.of(context).load("assets/images/carro.png");
    return data.buffer.asUint8List();
  }

  void currenLocationCar() async {
    try {
      Uint8List imagenData = await getMarketCar();
      var location = await _tracker.getLocation();
      GoogleMapController controller = await _controllerGoogle.future;
      actualizarMarkert(location, imagenData);
      if (_streamSubCriptor != null) {
        _streamSubCriptor.cancel();
      }
      _streamSubCriptor = _tracker.onLocationChanged.listen((event) {
        if (_controllerGoogle != null) {
          controller.animateCamera(
            CameraUpdate.newCameraPosition(
              new CameraPosition(
                bearing: 100,
                target: LatLng(event.latitude, event.longitude),
                tilt: 0,
                zoom: 18
              ),
            ),
          );
          actualizarMarkert(event, imagenData);
        }
      });
    } on PlatformException catch (e) {
      print("curren Location CAr------------------${e.code}");
    }
  }
}
